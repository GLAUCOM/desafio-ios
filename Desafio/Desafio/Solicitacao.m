//
//  Solicitacao.m
//  Desafio
//
//  Created by Glauco Moraes on 06/05/15.
//  Copyright (c) 2015 Mackenzie. All rights reserved.
//

#import "Solicitacao.h"

@interface Solicitacao()

@property (strong, nonatomic) UIView *sobreposicao;

@property (strong, nonatomic) AFHTTPClient *mapeamento;

- (void)mostraSobreposicao;

- (void)removeSobreposicao;

@end

@implementation Solicitacao

//+(instancetype)sharedInstance {
//    static dispatch_once_t pred;
//    static id __singleton = nil;
//    
//    dispatch_once(&pred, ^{ __singleton = [[self alloc] init]; });
//    return __singleton;
//}

- (void)mostraSobreposicao
{
    if(!self.sobreposicao) {
        
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        
        self.sobreposicao = [[UIView alloc] initWithFrame:screenRect];
        
        [self.sobreposicao setBackgroundColor:[UIColor clearColor]];
        //ActivityIndicator
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        //TabBarControlller
        UITabBarController *tabBarController = [UIApplication sharedApplication].keyWindow.window.rootViewController.tabBarController;
        //NavigationController
        UINavigationController *nav = [UIApplication sharedApplication].keyWindow.window.rootViewController.navigationController;
        
        if (IDIOM) {
            
            float navigationBarHeight = [[nav navigationBar] frame].size.height;
            
            float tabBarHeight = [[tabBarController tabBar] frame].size.height;
            
            [indicator setCenter:CGPointMake(self.sobreposicao.frame.size.width / 2.0, (self.sobreposicao.frame.size.height  - navigationBarHeight - tabBarHeight) / 2.0)];
            [indicator startAnimating];
            
        }else{
            
            float navigationBarHeight = [[nav navigationBar] frame].size.height;
            
            float tabBarHeight = [[tabBarController tabBar] frame].size.height;
            
            [indicator setCenter:CGPointMake(self.sobreposicao.frame.size.width / 2.0 , (self.sobreposicao.frame.size.height  - navigationBarHeight - tabBarHeight) / 2.0 + 108)];
            [indicator startAnimating];
        }
        
        
        [self.sobreposicao addSubview:indicator];
    }
    
    [[UIApplication sharedApplication].keyWindow addSubview:self.sobreposicao];
}


- (void)removeSobreposicao
{
    if (self.sobreposicao) {
        [self.sobreposicao removeFromSuperview];
    }
}

#pragma mark - Métodos de Requisição

-(id)init {
    
    NSString *URL = [NSString stringWithFormat:@"https://%@", URL_SOBRE];
    
    if ([RKObjectManager sharedManager] == nil) {
        
        _gerenciador = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:URL]];
        
        [RKObjectManager setSharedManager: _gerenciador];
        
    } else {
        
        _gerenciador = [RKObjectManager sharedManager];
        
    }
    
    _gerenciador = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:URL]];
    
    [RKObjectManager setSharedManager: _gerenciador];
    
    [RKMIMETypeSerialization registerClass:[RKNSJSONSerialization class] forMIMEType:@"text/javascript"];
    
    [RKMIMETypeSerialization registerClass:[RKNSJSONSerialization class] forMIMEType:@"application/json"];
    
    return self;
}

-(void) performRequestWithoutMappingWithParams:(NSDictionary *) params method:(RKRequestMethod)method path:(NSString *)path withSuccesBlock:(void(^)(id responseObject)) successBlock andErrorBlock:(void(^)(NSError * error)) errorBlock{
    
    NSString *methodName = @"";
    
    switch (method) {
        case RKRequestMethodPOST:
            methodName = @"POST";
            break;
        case RKRequestMethodGET:
            methodName = @"GET";
            break;
        case RKRequestMethodPUT:
            methodName = @"PUT";
            break;
        case RKRequestMethodDELETE:
            methodName = @"DELETE";
            break;
        default:
            methodName = @"GET";
            break;
    }
    __block Solicitacao *innerSelf = self;
    
    NSURLRequest *requisicao = [self.mapeamento requestWithMethod:methodName path:path parameters:params];
    
    AFHTTPRequestOperation *operacao = [[AFHTTPRequestOperation alloc]initWithRequest:requisicao];
    
    [operacao setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operacao, id responseObject) {
        
        if (successBlock) {
            
            successBlock(responseObject);
            
        }
        
        [innerSelf removeSobreposicao];
        
    } failure:^(AFHTTPRequestOperation *operacao, NSError *erro) {
        
        if (errorBlock) {
            
            errorBlock(erro);
            
        }
        
        [innerSelf removeSobreposicao];
    }];
    
    [self mostraSobreposicao];
    
    [self.mapeamento enqueueHTTPRequestOperation:operacao];
}

-(void) addOperationToQueue:(RKObjectRequestOperation *) operacao {
    
    [_gerenciador enqueueObjectRequestOperation:operacao];
    
    [self mostraSobreposicao];
}

-(RKObjectRequestOperation *) createRequestOperationWithResponseDescriptors:(NSArray *) responseDescriptors andRequest:(NSURLRequest *)request successBlock:(void (^)(NSArray *results))successBlock andErrorBlock:(void (^)(NSError *error))errorBlock; {
    
    RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:responseDescriptors];
    
    
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        if (successBlock) {
            
            successBlock([mappingResult array]);
        }
        
        [self removeSobreposicao];
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        if (errorBlock) {
            errorBlock(error);
        }
        [self removeSobreposicao];
    }];
    
    return operation;
}

-(NSURLRequest *)createRequestWithObject:(id)object HTTPMethod:(RKRequestMethod)method path:(NSString *)path parameters:(NSDictionary *)parametros {
    
    NSURLRequest *requisicao = [_gerenciador requestWithObject:object method:method path:path parameters:parametros];
    
    return requisicao;
}

@end