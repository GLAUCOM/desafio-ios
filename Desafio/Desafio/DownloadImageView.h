//
//  DownloadImageView.h
//  Desafio
//
//  Created by Glauco Moraes on 05/05/15.
//  Copyright (c) 2015 Mackenzie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DownloadImageView : UIImageView {
    NSString *url;
    UIActivityIndicatorView *progress;
    NSOperationQueue *queue;
}

@property (nonatomic, copy) NSString *url;

@end
