//
//  Solicitacao.h
//  Desafio
//
//  Created by Glauco Moraes on 06/05/15.
//  Copyright (c) 2015 Mackenzie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
//#import <UIKit/UIImageView.h>
//#import <AFNetworking/AFNetworking.h>
//#import <UIImageView+AFNetworking.h>

#define URL_SOBRE @"http://api.dribbble.com/shots/"
#define IDIOM (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

//typedef enum {
//    DribbleShotTypeDebuts = 0,
//    DribbleShotTypeEveryone,
//    DribbleShotTypePopular
//} DribbleShotType;

@interface Solicitacao : NSObject

//+ (instancetype)sharedInstance;
//- (void)getShotsOfType:(DribbleShotType)dribbleShotType
//                onPage:(NSInteger)page
//               success:(void (^)(NSArray *))success
//               failure:(void (^)(NSError *))failure;
//- (void)setImageWithURLString:(NSString *)urlString
//                 forImageView:(UIImageView *)imageView;

@property(strong, nonatomic, readonly) RKObjectManager *gerenciador;

-(void) addOperationToQueue:(RKObjectRequestOperation *) operacao;

-(RKObjectRequestOperation *) createRequestOperationWithResponseDescriptors:(NSArray *) responseDescriptors andRequest:(NSURLRequest *)request successBlock:(void (^)(NSArray *results))successBlock andErrorBlock:(void (^)(NSError *error))errorBlock;

-(NSURLRequest *)createRequestWithObject:(id) object HTTPMethod:(RKRequestMethod) method path:(NSString *)path parameters:(NSDictionary *) parametros;

-(void)performRequestWithoutMappingWithParams:(NSDictionary *) params method:(RKRequestMethod)method path:(NSString *)path withSuccesBlock:(void(^)(id responseObject)) successBlock andErrorBlock:(void(^)(NSError * error)) errorBlock;


@end
