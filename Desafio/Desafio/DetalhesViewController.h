//
//  DetalhesViewController.h
//  Desafio
//
//  Created by Glauco Moraes on 06/05/15.
//  Copyright (c) 2015 Mackenzie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
//#import "DownloadImageView.h"

@interface DetalhesViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *detalhesImagem;

@property (strong, nonatomic) IBOutlet UIImageView *detalheMiniatura;

@property (strong, nonatomic) IBOutlet UILabel *detalheNome;

@property (strong, nonatomic) IBOutlet UILabel *detalheUsuario;

@property (strong, nonatomic) IBOutlet UITextView *detalheDescricao;

@end
