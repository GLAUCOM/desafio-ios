//
//  AppDelegate.h
//  Desafio
//
//  Created by Glauco Moraes on 05/05/15.
//  Copyright (c) 2015 Mackenzie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

