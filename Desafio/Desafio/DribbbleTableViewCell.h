//
//  DribbbleTableViewCell.h
//  Desafio
//
//  Created by Glauco Moraes on 05/05/15.
//  Copyright (c) 2015 Mackenzie. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <Foundation/Foundation.h>
//#import "DownloadImageView.h"

// Classe filha de UITableViewCell

@interface DribbbleTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imageDribbble;

@property (strong, nonatomic) IBOutlet UILabel *likesLabel;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) IBOutlet UILabel *playerNameLabel;

@end
